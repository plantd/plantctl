#!/bin/bash

#
# Currently tested on Debian/Raspbian 10 and Arch
#
# Install just `plantctl`:
# $ curl https://gitlab.com/snippets/1865975/raw | sudo bash
#
# Install `plantctl` with all plugins:
# $ curl https://gitlab.com/snippets/1865975/raw | sudo bash -s -- -a
#

set -e

#
# Check OS and version
#

# XXX: not all of these are supported/tested yet, just Debian, Raspbian and Arch
if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    OS=$(lsb_release -si)
    VER=$(lsb_release -sr)
    echo "$OS is unsupported"
    exit 1
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
    echo "$OS is unsupported"
    exit 1
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS=Debian
    VER=$(cat /etc/debian_version)
elif [ -f /etc/SuSe-release ]; then
    echo "$OS is unsupported"
    exit 1
elif [ -f /etc/redhat-release ]; then
    echo "$OS is unsupported"
    exit 1
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
    echo "$OS is unsupported"
    exit 1
fi

#
# Process input arguments
#

while getopts hads-: arg; do
    case $arg in
        h )
             echo "Usage:"
             echo "    $0 -h                      Display this help message."
             echo "    $0 -d|--docker             Install the Docker plugin."
             echo "    $0 -s|--systemd            Install the Systemd plugin."
             echo "    $0 -a|--all                Install all plugins."
             exit 0
             ;;
        a )  ARG_A=true ;;
        d )  ARG_D=true ;;
        s )  ARG_S=true ;;
        - )  LONG_OPTARG="${OPTARG#*=}"
             case $OPTARG in
                 all     )  ARG_A=true ;;
                 docker  )  ARG_D=true ;;
                 systemd )  ARG_S=true ;;
                 all* | docker* | systemd* )
                            echo "No arg allowed for --$OPTARG" >&2; exit 2 ;;
                 '' )       break ;; # "--" terminates argument processing
                 * )        echo "Illegal option --$OPTARG" >&2; exit 2 ;;
             esac ;;
        \? ) exit 2 ;;  # getopts already reported the illegal option
    esac
done
shift $((OPTIND-1)) # remove parsed options and args from $@ list

if [ "$ARG_A" == "true" ]; then
    ARG_D=true
    ARG_S=true
fi

#
# Install dependencies
#

deps=()

echo "Installing dependencies..."
if [ "$OS" == "Arch Linux" ]; then
    deps+=( git go make python python-pip )
    if [ "$ARG_D" == "true" ]; then
        deps+=()
    fi
    if [ "$ARG_S" == "true" ]; then
        deps+=( systemd-libs )
    fi
    pacman -S --noconfirm ${deps[@]}
elif [[ "$OS" == "Debian GNU/Linux" || "$OS" == "Debian" ]]; then
    deps+=( build-essential git golang-go python3 python3-pip )
    if [ "$ARG_D" == "true" ]; then
        deps+=()
    fi
    if [ "$ARG_S" == "true" ]; then
        deps+=( libsystemd-dev )
    fi
    apt-get install -y ${deps[@]}
fi

python3 -m pip install ansible
python3 -m pip install "python-dotenv[cli]"

#
# Install the things
#

echo "Setting up system directories..."
mkdir -p /usr/share/plantctl/plugins
mkdir -p /etc/plantd

echo "Installing plantctl..."
git clone https://gitlab.com/plantd/plantctl
cd plantctl
make && make install

echo "Installing plantctl plugins..."
if [ "$ARG_D" == "true" ]; then
    make plugin-docker && make install-plugin-docker
fi
if [ "$ARG_S" == "true" ]; then
    make plugin-systemd && make install-plugin-systemd
fi

echo "Copying sample configuration to /etc/plantd/plantctl.yaml"
cp configs/v1/plantctl.yaml /etc/plantd/
