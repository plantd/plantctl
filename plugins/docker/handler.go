package main

import (
	"context"
	"fmt"
	//"log"
	//"os"
	"sort"
	//"time"

	ctx "gitlab.com/plantd/plantctl/pkg/context"
	"gitlab.com/plantd/plantctl/pkg/model"
	pb "gitlab.com/plantd/plantctl/proto"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	//"github.com/gosuri/uitable"
	. "github.com/logrusorgru/aurora"
)

type handler string

func connect() (cli *client.Client) {
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		panic(err)
	}

	return
}

func services() (units []model.Unit) {
	c, _ := ctx.LoadConfig()

	for name, u := range c.Services {
		target := name + ".service"
		units = append(units, model.Unit{u.Order, target, u.Watch, u.Control})
	}

	// Make the unit list
	for name, u := range c.Units {
		target := "plantd-unit@" + name + ".service"
		units = append(units, model.Unit{u.Order, target, u.Watch, u.Control})
	}

	for name, u := range c.Modules {
		target := "plantd-module@" + name + ".service"
		units = append(units, model.Unit{u.Order, target, u.Watch, u.Control})
	}

	return
}

func (h handler) Start() {
	cli := connect()

	fmt.Println("waaaa")

	containers, err := cli.ContainerList(context.Background(), types.ContainerListOptions{})
	if err != nil {
		panic(err)
	}

	for _, container := range containers {
		fmt.Printf("%s %s\n", container.ID[:10], container.Image)
	}
}

func (h handler) Stop() {
	//cli := connect()
}

func (h handler) Restart() {
	h.Stop()
	h.Start()
}

func (h handler) Status() {
	//cli := connect()
}

func (h handler) GetStatus() []*pb.Service {
	//cli := connect()

	unitResponses := []*pb.Service{}

	return unitResponses
}

func (h handler) Watch() {
}

func index(vs []model.Unit, t string) int {
	for i, v := range vs {
		if v.Target == t {
			return i
		}
	}
	return -1
}

func filter(vs []model.Unit, f func(model.Unit) bool) []model.Unit {
	vsf := make([]model.Unit, 0)
	for _, v := range vs {
		if f(v) {
			vsf = append(vsf, v)
		}
	}
	return vsf
}

func getColor(id string) (c Color) {
	units := services()
	// alphabetic sort
	sort.Slice(units, func(i, j int) bool {
		return units[i].Target < units[j].Target
	})
	pos := index(filter(units, func(v model.Unit) bool { return v.Watch }), id) % 6
	switch pos {
	case 0:
		c = RedFg
	case 1:
		c = GreenFg
	case 2:
		c = YellowFg
	case 3:
		c = BlueFg
	case 4:
		c = MagentaFg
	case 5:
		c = CyanFg
	}

	return
}

/*
 *func logFormatter(entry *sd.JournalEntry) (string, error) {
 *    id := entry.Fields["_SYSTEMD_UNIT"]
 *    return fmt.Sprintf("[%s] %s\n",
 *        Colorize(id, getColor(id)),
 *        entry.Fields["MESSAGE"]), nil
 *}
 */

// exported
var Handler handler
