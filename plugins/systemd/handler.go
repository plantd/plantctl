package main

import (
	"fmt"
	"log"
	"os"
	"sort"
	"time"

	"gitlab.com/plantd/plantctl/pkg/context"
	"gitlab.com/plantd/plantctl/pkg/model"
	pb "gitlab.com/plantd/plantctl/proto"

	"github.com/coreos/go-systemd/dbus"
	sd "github.com/coreos/go-systemd/sdjournal"
	"github.com/coreos/go-systemd/util"
	"github.com/gosuri/uitable"
	. "github.com/logrusorgru/aurora"
)

// to build:
// go build -buildmode=plugin -o bin/systemd-handler.so pkg/handler/systemd/handler.go

type handler string

func connect() (conn *dbus.Conn) {
	if !util.IsRunningSystemd() {
		log.Fatal("This system isn't running Systemd")
	}

	conn, err := dbus.NewSystemdConnection()
	if err != nil {
		log.Fatalf("failed to connect to Systemd DBus: %s\n", err)
	}

	return
}

func services() (units []model.Unit) {
	c, _ := context.LoadConfig()

	// Make the unit list
	for name, u := range c.Modules {
		target := "plantd-module@" + name + ".service"
		units = append(units, model.Unit{u.Order, target, u.Watch, u.Control})
	}

	for name, u := range c.Units {
		target := "plantd-unit@" + name + ".service"
		units = append(units, model.Unit{u.Order, target, u.Watch, u.Control})
	}

	for name, u := range c.Services {
		target := name + ".service"
		units = append(units, model.Unit{u.Order, target, u.Watch, u.Control})
	}

	return
}

func (h handler) Start() {
	conn := connect()
	defer conn.Close()

	units := services()

	reschan := make(chan string)

	// sort for startup
	sort.Slice(units, func(i, j int) bool {
		return units[i].Order < units[j].Order
	})

	for _, u := range units {
		if u.Control {
			fmt.Println(Green("Starting"), u.Target)
			if _, err := conn.StartUnit(u.Target, "replace", reschan); err != nil {
				log.Fatalf("failed to stop %s: %s\n", u.Target, err)
			}

			job := <-reschan
			if job != "done" {
				log.Fatal("job is not done:", job)
			}
		}
	}
}

func (h handler) Stop() {
	conn := connect()
	defer conn.Close()

	units := services()

	reschan := make(chan string)

	// reverse sort on shutdown
	sort.Slice(units, func(i, j int) bool {
		return units[i].Order > units[j].Order
	})

	// Stop everything
	for _, u := range units {
		if u.Control {
			fmt.Println(Red("Stopping"), u.Target)
			if _, err := conn.StopUnit(u.Target, "replace", reschan); err != nil {
				log.Fatalf("failed to stop %s: %s\n", u.Target, err)
			}

			job := <-reschan
			if job != "done" {
				log.Fatal("job is not done:", job)
			}
		}
	}
}

func (h handler) Restart() {
	h.Stop()
	h.Start()
}

func (h handler) Status() {
	conn := connect()
	defer conn.Close()

	units := services()

	table := uitable.New()
	table.MaxColWidth = 80

	// Check the status of everything
	for _, u := range units {
		if u.Control {
			properties, _ := conn.GetUnitProperties(u.Target)
			if properties["SubState"] == "running" {
				table.AddRow(properties["Id"], Green(properties["SubState"]))
			} else {
				table.AddRow(properties["Id"], Red(properties["SubState"]))
			}
		}
	}
	fmt.Println(table)
}

func (h handler) GetStatus() []*pb.Service {
	fmt.Println("Getting statuses for server...")
	conn := connect()
	defer conn.Close()

	unitResponses := []*pb.Service{}

	units := services()
	// Check the status of everything
	for _, u := range units {
		if u.Control {
			properties, _ := conn.GetUnitProperties(u.Target)
			switch properties["SubState"] {
			case "running":
				unitResponses = append(unitResponses, &pb.Service{
					Target: properties["Id"].(string),
					State:  pb.Service_RUNNING,
				})
			case "dead":
				unitResponses = append(unitResponses, &pb.Service{
					Target: properties["Id"].(string),
					State:  pb.Service_STOPPED,
				})
			default:
				unitResponses = append(unitResponses, &pb.Service{
					Target: properties["Id"].(string),
					State:  pb.Service_DEFAULT,
				})
			}
		}
	}

	return unitResponses
}

func (h handler) Watch() {
	var matches []sd.Match
	units := services()

	field := sd.SD_JOURNAL_FIELD_SYSTEMD_UNIT
	for _, u := range units {
		if u.Watch {
			matches = append(matches, sd.Match{Field: field, Value: u.Target})
		}
	}

	r, err := sd.NewJournalReader(sd.JournalReaderConfig{
		Since:     time.Duration(-15) * time.Second,
		Matches:   matches,
		Formatter: logFormatter,
	})

	if err != nil {
		log.Fatalf("Error opening journal: %s", err)
	}

	if r == nil {
		log.Fatal("Got a nil reader")
	}

	defer r.Close()

	// and follow the reader synchronously
	timeout := time.Duration(15) * time.Minute
	if err = r.Follow(time.After(timeout), os.Stdout); err != sd.ErrExpired {
		log.Fatalf("Error during follow: %s", err)
	}
}

func index(vs []model.Unit, t string) int {
	for i, v := range vs {
		if v.Target == t {
			return i
		}
	}
	return -1
}

func filter(vs []model.Unit, f func(model.Unit) bool) []model.Unit {
	vsf := make([]model.Unit, 0)
	for _, v := range vs {
		if f(v) {
			vsf = append(vsf, v)
		}
	}
	return vsf
}

func getColor(id string) (c Color) {
	units := services()
	// alphabetic sort
	sort.Slice(units, func(i, j int) bool {
		return units[i].Target < units[j].Target
	})
	pos := index(filter(units, func(v model.Unit) bool { return v.Watch }), id) % 6
	switch pos {
	case 0:
		c = RedFg
	case 1:
		c = GreenFg
	case 2:
		c = YellowFg
	case 3:
		c = BlueFg
	case 4:
		c = MagentaFg
	case 5:
		c = CyanFg
	}

	return
}

func logFormatter(entry *sd.JournalEntry) (string, error) {
	id := entry.Fields["_SYSTEMD_UNIT"]
	return fmt.Sprintf("[%s] %s\n",
		Colorize(id, getColor(id)),
		entry.Fields["MESSAGE"]), nil
}

// TODO: make a better log formatter
/*
 *func logFormatter(entry *sd.JournalEntry) (string, error) {
 *    color := theme.Colors["Message"]
 *    switch entry.Fields["PRIORITY"] {
 *    case "0":
 *        fallthrough
 *    case "1":
 *        fallthrough
 *    case "2":
 *        fallthrough
 *    case "3":
 *        color = theme.Colors["Error"]
 *    case "4":
 *        color = theme.Colors["Warning"]
 *    case "5":
 *        color = theme.Colors["Notice"]
 *    }
 *
 *    return fmt.Sprintf("[%s]%s [%s]%s [%s]%s\n",
 *        theme.Colors["Timestamp"],
 *        time.Unix(0, int64(entry.RealtimeTimestamp)*int64(time.Microsecond)).Format("Jan 02 15:04:05"),
 *        theme.Colors["Service"],
 *        entry.Fields["SYSLOG_IDENTIFIER"],
 *        color,
 *        entry.Fields["MESSAGE"]), nil
 *}
 */

// exported
var Handler handler
