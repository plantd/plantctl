package cmd

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"github.com/joho/godotenv"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	git "gopkg.in/src-d/go-git.v4"
)

// installCmd represents the install command
var installCmd = &cobra.Command{
	Use:   "install",
	Short: "install plantd services",
	Run:   install,
}

// Ask the user for confirmation.
// Deprecated: ?
func Ask(s string) bool {
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Printf("%s [y/n]: ", s)

		response, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		response = strings.ToLower(strings.TrimSpace(response))

		if response == "y" || response == "yes" {
			return true
		} else if response == "n" || response == "no" {
			return false
		}
	}
}

func parseVarsFile(filename string, tmpName string) {
	// filename must be the full path of the default config file
	// tmpName is the full path for a temporary file, e.g. "./.tmp.yml"

	inFile, err := os.Open(filename)
	if err != nil {
		log.Fatal("Unable to open vars file", err)
	}
	defer inFile.Close()

	outFile, err := os.Create(tmpName)
	if err != nil {
		log.Fatal("Unable to open temporary file", err)
	}
	defer outFile.Close()

	scanner := bufio.NewScanner(inFile)
	writer := bufio.NewWriter(outFile)
	stdin := bufio.NewReader(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()

		// current line is not a leaf
		if strings.HasSuffix(line, ":") {
			_, _ = writer.WriteString(line)
			_, _ = writer.WriteString("\n")
			fmt.Println(line)
			continue
		}

		// current line is commented out
		if strings.HasPrefix(line, "#") {
			_, _ = writer.WriteString(line)
			_, _ = writer.WriteString("\n")
			fmt.Println(line)
			continue
		}

		// current line empty
		if line == "" {
			_, _ = writer.WriteString(line)
			_, _ = writer.WriteString("\n")
			fmt.Println(line)
			continue
		}

		split := strings.Split(line, ":")

		// expect split to be [key, value], i.e. length 2
		// note that the key contains indentation white spaces
		if len(split) != 2 {
			_, _ = writer.WriteString(line)
			_, _ = writer.WriteString("\n")
			continue
		}

		// parse for indentation, key and value
		key := strings.Trim(split[0], " ")
		indent := len(split[0]) - len(strings.TrimLeft(split[0], " "))
		val := strings.TrimLeft(split[1], " ")

		// Ask for user input and parse it
		fmt.Printf("%v%v: (%v) [enter value to change] ",
			strings.Repeat(" ", indent), key, val)
		input, err := stdin.ReadString('\n')
		if err != nil {
			log.Fatal("Unable to read user input", err)
		}
		input = strings.TrimSuffix(input, "\n")
		// empty input means leave the value unchanged
		if input == "" {
			input = val
		}

		_, _ = writer.WriteString(strings.Repeat(" ", indent))
		_, _ = writer.WriteString(fmt.Sprintf("%v: %v\n", key, input))
	}

	if err := writer.Flush(); err != nil {
		log.Println("a non-fatal error occurred while flushing the buffer")
	}
}

func install(cmd *cobra.Command, args []string) {
	// parse flags
	flagInstallConfigFile, _ := cmd.Flags().GetString("install-config-file")
	dotenvFile := viper.GetString("dotenv")

	// Clone
	url := "https://gitlab.com/crdc/apex/ansible.git"
	cache, _ := os.UserCacheDir()
	sep := string(os.PathSeparator)
	directory := cache + sep + "plantd" + sep + "ansible"
	_, err := git.PlainClone(directory, false, &git.CloneOptions{
		URL:               url,
		RecurseSubmodules: git.DefaultSubmoduleRecursionDepth,
	})

	if err != nil {
		switch err {
		case git.ErrRepositoryAlreadyExists:
			// the repo is nil in this case. We need to open it again.
			var err2 error
			repo, err2 := git.PlainOpen(directory)
			if err2 != nil {
				log.Fatal("failed to open git repository", err2)
			}
			if err2 = repo.Fetch(&git.FetchOptions{}); err2 != nil {
				log.Fatal("failed to fetch git repository", err2)
			}
			worktree, _ := repo.Worktree()
			if err2 = worktree.Pull(&git.PullOptions{}); err2 != nil {
				log.Fatal("failed to pull git repository", err2)
			}
			// If changes have been made to the local branch that are
			// not present for origin, discard them.
			if err2 = worktree.Reset(&git.ResetOptions{Mode: git.HardReset}); err2 != nil {
				log.Fatal("failed to pull git repository", err2)
			}
		default:
			log.Fatal("failed to clone:", err)
		}
	}

	// Configure
	varsFilePath := directory + sep + "vars" + sep + "main.yml"
	if flagInstallConfigFile == "" {
		// prompt the user for input via stdin
		tmpFilePath := "./.tmp.yml"
		parseVarsFile(varsFilePath, tmpFilePath)
		err = os.Rename(tmpFilePath, varsFilePath)
		if err != nil {
			log.Fatal("Could not copy user input into vars file", err)
		}
	} else if flagInstallConfigFile == "use-default" {
		// do nothing
	} else {
		// copy user specified file to cloned repo
		dest, err := os.Create(varsFilePath)
		if err != nil {
			log.Fatal("Unable to create dest file", err)
		}
		defer dest.Close()

		src, err := os.Open(flagInstallConfigFile)
		if err != nil {
			log.Fatal("Unable to open src file", err)
		}
		defer src.Close()

		if _, err = io.Copy(dest, src); err != nil {
			log.Fatal("Unable to copy install-config-file", err)
		}
	}

	// Set up ansible stuff
	hosts := directory + sep + "inventory/local"
	playbook := directory + sep + "playbook.yml"

	// This is necessary to allow postgres to set a plaintext password
	os.Setenv("ANSIBLE_CONFIG", directory+sep+"ansible.cfg")

	// Execute playbook
	err = godotenv.Exec([]string{dotenvFile}, "ansible-playbook",
		[]string{"-i", hosts, "-K", playbook})
	if err != nil {
		log.Fatal("running playbook failed:", err)
	}
}
