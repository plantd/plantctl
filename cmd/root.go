package cmd

import (
	"fmt"
	"log"
	"os"
	"plugin"

	"gitlab.com/plantd/plantctl/pkg/context"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile string
	//daemon     bool
	pluginName string
	handler    context.Handler
	Config     *context.Config
	Verbose    bool

	rootCmd = &cobra.Command{
		Use:   "plantctl",
		Short: "Application to control plantd services",
		Long:  `A control utility for interacting with plantd services.`,
	}
)

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	addCommands()

	// Setup command flags
	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file (default is $HOME/.config/plantd/plantctl.yaml)")
	rootCmd.PersistentFlags().StringVarP(&pluginName, "plugin", "p", "systemd", "name of the plugin to load")
	rootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "verbose output")
	rootCmd.PersistentFlags().StringP("dotenv", "e", ".env", ".env file to use")

	installCmd.Flags().StringP("install-config-file", "f", "", "path to config file or \"use-default\"")

	_ = viper.BindPFlag("plugin", rootCmd.PersistentFlags().Lookup("plugin"))
	_ = viper.BindPFlag("verbose", rootCmd.PersistentFlags().Lookup("verbose"))
	_ = viper.BindPFlag("dotenv", rootCmd.PersistentFlags().Lookup("dotenv"))

	viper.SetDefault("verbose", false)
}

func addCommands() {
	// Commands for managing installations
	rootCmd.AddCommand(installCmd)
	//rootCmd.AddCommand(uninstallCmd)
	//rootCmd.AddCommand(updateCmd)
	rootCmd.AddCommand(patchCmd)
	rootCmd.AddCommand(deployCmd)

	// TODO: add `completions` command to generate bash/zsh files
	//rootCmd.GenBashCompletionFile("test_completions.sh")

	// Commands for working with services, all rely on a plugin being loaded
	rootCmd.AddCommand(restartCmd)
	rootCmd.AddCommand(serverCmd)
	rootCmd.AddCommand(startCmd)
	rootCmd.AddCommand(statusCmd)
	rootCmd.AddCommand(stopCmd)
	rootCmd.AddCommand(watchCmd)
	rootCmd.AddCommand(pingCmd)

	// Miscellaneous commands
	rootCmd.AddCommand(versionCmd)
}

// This should be called by any subcommand that uses a plugin
func loadPlugin() {
	// TODO: use XDG or something platform independent
	// Load module
	pluginSo := "/usr/share/plantctl/plugins/" + pluginName + ".so"
	if _, err := os.Stat(pluginSo); os.IsNotExist(err) {
		panic("failed to load plugin: " + pluginSo)
	} else {
		plug, err := plugin.Open(pluginSo)
		if err != nil {
			panic(err)
		}
		// Lookup symbol
		symHandler, err := plug.Lookup("Handler")
		if err != nil {
			panic(err)
		}

		handler = symHandler.(context.Handler)

		// Build complains if you don't touch this
		if handler == nil {
			panic("something went horribly wrong")
		}
	}
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		//viper.SetConfigFile(cfgFile)
		// TODO: handle this correctly
		log.Println("!!! This isn't handled, not using file", cfgFile)
	}

	Config, err := context.LoadConfig()
	if err != nil {
		_ = fmt.Errorf("Fatal error reading config file: %s \n", err)
	}

	if Config == nil {
		_ = fmt.Errorf("wtf")
	}
}
