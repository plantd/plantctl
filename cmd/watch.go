package cmd

import (
	"github.com/spf13/cobra"
)

// watchCmd represents the watch command
var watchCmd = &cobra.Command{
	Use:   "watch",
	Short: "watch plantd services",
	Run: func(cmd *cobra.Command, args []string) {
		loadPlugin()
		handler.Watch()
	},
}
