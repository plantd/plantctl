package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/joho/godotenv"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	git "gopkg.in/src-d/go-git.v4"
)

// patchCmd represents the patch command
var patchCmd = &cobra.Command{
	Use:   "patch",
	Short: "apply a patch",
	Args:  cobra.ExactArgs(1),
	Run:   patch,
}

func patch(cmd *cobra.Command, args []string) {

	// parse argument
	patchName := args[0]
	fmt.Println("patch name: ", patchName)

	// parse flag
	env := viper.GetString("dotenv")

	// Clone the patch repo
	url := "https://gitlab.com/plantd/patches/" + patchName
	if !strings.HasSuffix(url, ".git") {
		url += ".git"
	}

	fmt.Println("patch url: ", url)
	cache, _ := os.UserCacheDir()
	sep := string(os.PathSeparator)
	directory := cache + sep + "plantd" + sep + "patches" + patchName
	_, err := git.PlainClone(directory, false, &git.CloneOptions{
		URL:               url,
		RecurseSubmodules: git.DefaultSubmoduleRecursionDepth,
	})

	if err != nil {
		switch err {
		case git.ErrRepositoryAlreadyExists:
			// the repo is nil in this case. We need to open it again.
			repo, err2 := git.PlainOpen(directory)
			if err2 != nil {
				log.Fatal("failed to open git repository", err2)
			}
			if err2 = repo.Fetch(&git.FetchOptions{}); err2 != nil {
				log.Fatal("failed to fetch git repository", err2)
			}
			worktree, _ := repo.Worktree()
			if err2 = worktree.Pull(&git.PullOptions{}); err2 != nil {
				log.Fatal("failed to pull git work tree", err2)
			}
			// If changes have been made to the local branch that are
			// not present for origin, discard them.
			if err2 = worktree.Reset(&git.ResetOptions{Mode: git.HardReset}); err2 != nil {
				log.Fatal("failed to reset local branch", err2)
			}
		default:
			log.Fatal("failed to clone:", err)
		}
	}

	// Set up ansible stuff
	hosts := directory + sep + "hosts"
	playbook := directory + sep + "patch.yml"

	// Execute playbook
	err = godotenv.Exec([]string{env}, "ansible-playbook",
		[]string{"-i", hosts, "-K", playbook})
	if err != nil {
		log.Fatal("running playbook failed:", err)
	}
}
