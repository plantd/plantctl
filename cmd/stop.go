package cmd

import (
	"github.com/spf13/cobra"
)

// stopCmd represents the stop command
var stopCmd = &cobra.Command{
	Use:   "stop",
	Short: "stop plantd services",
	Run: func(cmd *cobra.Command, args []string) {
		loadPlugin()
		handler.Stop()
	},
}
