package cmd

import (
	"fmt"

	"gitlab.com/plantd/plantctl/pkg/context"

	"github.com/spf13/cobra"
)

// pingCmd represents the ping command
var (
	port int
	host string

	pingCmd = &cobra.Command{
		Use:   "ping",
		Short: "Ping a plantctl manager service",
		Run:   ping,
	}
)

func init() {
	pingCmd.Flags().IntVarP(&port, "port", "", 5211, "the gRPC port to use")
	pingCmd.Flags().StringVarP(&host, "host", "", "localhost", "the gRPC host to use")
}

func ping(cmd *cobra.Command, args []string) {
	loadPlugin()

	c, err := context.NewManagerClient(host, port)
	if err != nil {
		_ = fmt.Errorf("error creating client: %s", err)
	}

	if err := c.Ping(); err != nil {
		_ = fmt.Errorf("failed to ping service: %s", err)
	}
}
