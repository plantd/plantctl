package cmd

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	git "gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/config"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/storage/memory"
)

var (
	prepare         bool
	finish          bool
	no_deploy_token bool
	branch          string

	deployCmd = &cobra.Command{
		Use:   "deploy",
		Short: "apply a deployment",
		Args:  cobra.ExactArgs(1),
		Run:   deploy,
	}
)

func init() {
	deployCmd.PersistentFlags().BoolVarP(&prepare, "prepare", "", false, "run prepare stage")
	deployCmd.PersistentFlags().BoolVarP(&finish, "finish", "", false, "run finish stage")
	deployCmd.PersistentFlags().StringVar(&branch, "branch", "master", "check out branch from deployment repo")
	deployCmd.PersistentFlags().BoolVar(&no_deploy_token, "no-deploy-token", false,
		"don't use deploy token from .env file to clone the deployment repo")
}

func deploy(cmd *cobra.Command, args []string) {

	// parse argument
	deploymentName := args[0]
	log.Debug("deployment name: ", deploymentName)

	// parse flag
	env := viper.GetString("dotenv")
	log.Debug(".env file: ", env)

	if !no_deploy_token {
		// load deployment tokens
		err := godotenv.Load(env)
		if err != nil {
			log.Fatal("Error loading .env file")
		}
	}

	uri, err := createUri(deploymentName, no_deploy_token)
	if err != nil {
		log.Panic(err)
	}

	// clone repo
	log.Infof("clone %s", uri)
	path, repo, err := cloneDeployment(deploymentName, uri)
	if err != nil {
		log.Fatal("Unable to clone deployment repo ", err)
	}

	// TODO: check if necessary files exist after clone

	// Check out the desired branch
	log.Debugf("Checking out branch %s", branch)
	err = checkoutBranch(branch, repo)
	if err != nil {
		log.Fatal("Unable to check out branch ", err)
	}

	// Set up ansible stuff
	sep := string(os.PathSeparator)
	hosts := path + sep + "inventory" + sep + "local"
	playbook := path + sep + "deployment.yml"

	// Use a local config if one exists
	// TODO: check if it exists before setting the variable
	os.Setenv("ANSIBLE_CONFIG", path+sep+"ansible.cfg")

	// run prepare stage if requested
	if prepare {
		log.Debug("run prepare")
		preparePlaybook := path + sep + "prepare.yml"
		err = godotenv.Exec(
			[]string{env},
			"ansible-playbook",
			[]string{"-i", hosts, "-K", preparePlaybook},
		)
		if err != nil {
			log.Fatal("running deployment preparation failed: ", err)
		}
	}

	// run deployment
	err = godotenv.Exec(
		[]string{env},
		"ansible-playbook",
		[]string{"-i", hosts, "-K", playbook}, // TODO: should add a flag for -K since it's not always needed
	)
	if err != nil {
		log.Fatal("running playbook failed: ", err)
	}

	// run finish stage if requested
	if finish {
		log.Debug("run finish")
		finishPlaybook := path + sep + "finish.yml"
		err = godotenv.Exec(
			[]string{env},
			"ansible-playbook",
			[]string{"-i", hosts, "-K", finishPlaybook},
		)
		if err != nil {
			log.Fatal("running deployment preparation failed: ", err)
		}
	}

	// cleanup cache?
}

// injectGitTokens parses the `repo` cli argument and injects a deploy token
// user/pass combination from your .env file. This allows accessing a private
// gitlab repository.
func injectGitTokens(uri string) (string, error) {

	tail := strings.Split(uri, "https://")[1]

	deploymentUser := os.Getenv("DEPLOY_TOKEN_USER")
	deploymentPass := os.Getenv("DEPLOY_TOKEN_PASS")

	newUri := fmt.Sprintf("https://%s:%s@%s", deploymentUser, deploymentPass, tail)
	return newUri, nil
}

func createUri(deployment string, no_deploy_tokens bool) (string, error) {

	tokenUri := ""
	// Inject the deploy tokens if necessary
	// This is only needed for isValidRepo
	if !no_deploy_tokens && strings.HasPrefix(deployment, "https://") {
		newUri, err := injectGitTokens(deployment)
		if err != nil {
			log.Panic(err)
		}
		tokenUri = newUri
	} else {
		tokenUri = deployment
	}

	// Check if the repository is valid
	if isRepoDeployment(deployment) {
		log.Debug(deployment + " is a deployment")
		uri := "https://gitlab.com/plantd/deployments/" + deployment
		return uri, nil
	} else if isValidRepo(tokenUri) {
		log.Debug(tokenUri + " is a clonable repo")
		return tokenUri, nil
	} else if isValidDir(deployment) {
		log.Debug(deployment + " is a valid path")
		return deployment, nil
	} else {
		return "", errors.New("not a valid deployment repository")
	}

}

func isRepoDeployment(deployment string) bool {
	url := "https://gitlab.com/plantd/deployments/" + deployment

	remote := git.NewRemote(
		memory.NewStorage(),
		&config.RemoteConfig{Name: deployment, URLs: []string{url}},
	)
	_, err := remote.List(&git.ListOptions{})
	if err != nil {
		log.Debug("didn't find deployment: " + err.Error())
		return false
	} else {
		return true
	}
}

func isValidRepo(deployment string) bool {
	remote := git.NewRemote(
		memory.NewStorage(),
		&config.RemoteConfig{Name: deployment, URLs: []string{deployment}},
	)
	_, err := remote.List(&git.ListOptions{})
	if err != nil {
		log.Debug("not a clonable repo: " + err.Error())
		return false
	} else {
		return true
	}
}

func isValidDir(deployment string) bool {
	if _, err := os.Stat(deployment); os.IsNotExist(err) {
		return false
	}
	return true
}

// cloneDeployment clones the deployment repo. If the repo already exists in the
// cache, changes are fetched, but not pulled. The reason is that for pulling
// the branch needs to be specified.
func cloneDeployment(deployment, uri string) (string, *git.Repository, error) {
	cache, _ := os.UserCacheDir()
	sep := string(os.PathSeparator)
	// can't have forward slashes in a path
	deployment = strings.Replace(deployment, "/", "\\", -1)
	directory := cache + sep + "plantd" + sep + "deployments" + sep + deployment
	repo, err := git.PlainClone(directory, false, &git.CloneOptions{
		URL:               uri,
		RecurseSubmodules: git.DefaultSubmoduleRecursionDepth,
	})

	if err != nil {
		switch err {
		case git.ErrRepositoryAlreadyExists:
			// The repo is nil in this case. We need to open it again.
			var err2 error
			repo, err2 = git.PlainOpen(directory)
			if err2 != nil {
				log.Debug("failed to open git repository: ", err2)
				return directory, repo, err2
			}

			// Fetch the latest changes
			err2 = repo.Fetch(&git.FetchOptions{})
			if err2 != nil {
				switch err2 {
				case git.NoErrAlreadyUpToDate:
					log.Debug("repository already up to date ", err2)
					return directory, repo, nil
				default:
					log.Debug("failed to fetch changes to repository ", err2)
					return directory, repo, err2
				}
			}
		default:
			log.Debug("failed to clone: ", err)
			return directory, repo, err
		}
	}
	// this return statement is only used when err == nil
	return directory, repo, err
}

// checkoutBranch checks out a specific branch of a git repo. If the branch
// already exists locally, the latest changes from the remote are pulled and
// all changed files are reset.
func checkoutBranch(branch string, repo *git.Repository) error {

	localBranch := plumbing.NewBranchReferenceName(branch)
	remoteBranch := plumbing.NewRemoteReferenceName("origin", branch)
	worktree, _ := repo.Worktree()

	// Try checking out local branch first
	err := worktree.Checkout(&git.CheckoutOptions{
		Branch: localBranch,
		Create: false,
		Force:  true,
	},
	)

	if err != nil {
		// Local branch does not exist. Checkout remote branch.
		log.Debugf("Local branch %s does not exist", localBranch)
		err = worktree.Checkout(&git.CheckoutOptions{
			Branch: remoteBranch,
			Create: false,
			Force:  true})
		if err != nil {
			log.Debugf("Could not checkout remote branch %s", remoteBranch)
			return err
		}

		// Map remote branch to local branch.
		err = worktree.Checkout(&git.CheckoutOptions{
			Branch: localBranch,
			Create: true,
			Force:  true})

		if err != nil {
			log.Debugf("Could not create local branch %s", localBranch)
			return err
		}
	} else {
		// Local Branch exists and has been checked out.
		// Pull from repo to make sure local branch is up to date.
		log.Debugf("Local branch %s exists", localBranch)
		err = worktree.Pull(&git.PullOptions{
			ReferenceName:     localBranch, // Use name of reference as stored by the remote
			SingleBranch:      true,
			Force:             false,
			RecurseSubmodules: git.DefaultSubmoduleRecursionDepth})
		switch err {
		case git.NoErrAlreadyUpToDate:
			log.Debugf("Branch %s is up to date", localBranch)
		case nil:
			log.Debugf("Pulled changes from remote to %s ", localBranch)
		default:
			log.Debugf("Failed to pull from remote branch %s", localBranch)
			return err
		}
		err = worktree.Reset(&git.ResetOptions{Mode: git.HardReset})
		if err != nil {
			log.Debugf("Unable to reset branch %s", localBranch)
			return err
		}

	}
	return nil
}
