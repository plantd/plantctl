package cmd

import (
	"github.com/spf13/cobra"
)

// restartCmd represents the restart command
var restartCmd = &cobra.Command{
	Use:   "restart",
	Short: "Restart plantd services",
	Run: func(cmd *cobra.Command, args []string) {
		loadPlugin()
		handler.Restart()
	},
}
