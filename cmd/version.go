package cmd

import (
	"fmt"

	"gitlab.com/plantd/plantctl/pkg"

	"github.com/spf13/cobra"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of plantctl",
	Long:  `Plantctl version information.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(pkg.VERSION)
	},
}
