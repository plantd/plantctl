package cmd

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/plantd/plantctl/pkg/context"
	"gitlab.com/plantd/plantctl/pkg/service"
	pb "gitlab.com/plantd/plantctl/proto"

	"github.com/spf13/cobra"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Start a server for managing services",
	Run:   server,
}

func server(cmd *cobra.Command, args []string) {
	loadPlugin()

	manager := service.NewManager(handler)

	c, _ := context.LoadConfig()

	// create the channel to listen on
	addr := fmt.Sprintf("%s:%d", c.Server.Bind, c.Server.Port)
	log.Println("Connect to", addr)
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("failed to listen: %s", err)
	}

	var opts []grpc.ServerOption

	// TODO: add TLS

	srv := grpc.NewServer(opts...)
	pb.RegisterManagerServer(srv, manager)
	reflection.Register(srv)
	if err := srv.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
