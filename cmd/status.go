package cmd

import (
	"github.com/spf13/cobra"
)

// statusCmd represents the status command
var statusCmd = &cobra.Command{
	Use:   "status",
	Short: "Check the status of plantd services",
	Run: func(cmd *cobra.Command, args []string) {
		loadPlugin()
		handler.Status()
	},
}
