package cmd

import (
	"github.com/spf13/cobra"
)

// startCmd represents the start command
var startCmd = &cobra.Command{
	Use:   "start",
	Short: "start plantd services",
	Run: func(cmd *cobra.Command, args []string) {
		loadPlugin()
		handler.Start()
	},
}
