[![Pipeline Status](https://gitlab.com/plantd/plantctl/badges/master/pipeline.svg)](https://gitlab.com/plantd/plantctl/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/plantd/plantctl)](https://goreportcard.com/report/gitlab.com/plantd/plantctl)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

---

# Plant Control

Utility for performing various `plantd` related functions including
performing an installation, and interacting with services, configurations,
deployments, and patches.

## Installing

### Script

A script is available to take care of installing the dependencies.

```sh
curl https://gitlab.com/snippets/1865975/raw | sudo bash
```

### Manual

If this is being done on Debian/Ubuntu the packages `golang-go` and
`libsystemd-dev` need to be installed using `apt`. For Arch the packages `go`
and `systemd-libs` should be installed. This is probable the best method to
use as the script and `Makefile` may lag behind development.

```sh
go get -u gitlab.com/plantd/plantctl
go build -o bin/plantctl gitlab.com/plantd/plantctl
sudo install -Dm 755 bin/plantctl /usr/bin/plantctl
```

#### Plugins

There are plugins for `systemd` and `docker` available to control services and
perform tasks such as `start`, `stop` and `restart`.

```sh
go get -u gitlab.com/plantd/plantctl/plugins/...
go build -buildmode=plugin -o target/systemd.so \
  gitlab.com/plantd/plantctl/plugins/systemd
go build -buildmod=plugin -o target/docker.so \
  gitlab.com/plantd/plantctl/plugins/docker
sudo mkdir -p /usr/share/plantctl/plugins
sudo install -Dm 644 bin/systemd.so /usr/share/plantctl/plugins/systemd.so
sudo install -Dm 644 bin/docker.so /usr/share/plantctl/plugins/docker.so
```

### Makefile

There is also a half baked `Makefile` that can be run with `make`.

```sh
make && sudo make install
```

## Developing

The `plantctl` gRPC service requires that the `protobuf` API be rebuilt if any
changes are made.

```sh
protoc --go_out=plugins=grpc:. proto/service.proto
```

<!-- TODO: Figure out the proper way to load plugins from `XDG` environment paths. -->

## Services

* `plantd-master`
* `plantd-configure`
* `plantd-broker`
* `plantd-experiment`
* `plantd-unit@`
* `plantd-module@`

## Configuration

Examples of configurations that controls startup/shutdown order, whether or
not a service should be managed, and whether or not a service should have log
messages displayed can be found in `configs`.

## Commands

### `plantctl`

#### Deployments

##### `deploy`

This command is used to perform a system deployment by applying an `ansible`
playbook with a specific structure that is pulled from a `git` repository kept
group, or in any `git` repository, or within a folder. For an example of how
the playbook must be structured see [here][sample-deployment].

```sh
plantctl deploy sample
plantctl deploy gitlab.com/plantd/deployments/sample
plantctl deploy /path/to/deployments/sample
```

##### `patch`

This command is meant to be used to patch an existing deployment by applying
`ansible` playbooks that are pulled from a `git` repository kept in the project
group, or in any `git` repository, or within a folder. For an example of how
the playbook must be structured see [here][sample-patch].

```sh
plantctl patch sample
plantctl patch gitlab.com/plantd/patches/sample
plantctl patch /path/to/patches/sample
```

#### Installation

##### `install`

[deprecated] Will be replaced by `deploy` in future releases.

The `install` command pulls the `ansible` repository and runs the setup steps
that it expects. There are some additional dependencies to make this happen.

```sh
sudo apt install python3-pip
python3 -m pip install ansible
python3 -m pip install "python-dotenv[cli]"
```

##### `update`

[deprecated] Will be replaced by `deploy` in future releases.

Currently unavailable.

##### `uninstall`

[deprecated] Will be replaced by `deploy` in future releases.

Currently unavailable.

#### Management

##### `start`

* use a plugin to execute start on all service files

##### `restart`

* use a plugin to execute restart on all service files

##### `status`

* use a plugin to execute status on all services files

##### `stop`

* use a plugin to execute stop on all services files

##### `watch`

* use a plugin to generate log output for troubleshooting/debugging

To make this more useful add the following to `$HOME/.jq`

```
def rekey(obj):
  . as $in
  | reduce (obj|keys_unsorted)[] as $k ({};
      if $in|has($k) then . + {($k): $in[$k]} else . end)
  | . + $in ;
```

and watch the logs with `sudo plantctl watch | jq 'rekey({ts,msg})'`.

## Roadmap

* [ ] deprecate `install` in favour of `deploy`
* [ ] drop `uninstall` and replace with `destroy`
* [ ] use `semver` packages to in CI
* [ ] cascade deployments to remote hosts from version 2 stack configuration
* [ ] cascade patches to remote hosts from version 2 stack configuration
* [ ] add plugin to configuration
* [ ] allow individual deployment stages to be run by adding the flag `--stages=foo,bar`
* [ ] remove `dotenv` flag, or shorten to `env`

[sample-deployments]: https://gitlab.com/plantd/deployments/sample.
[sample-patch]: https://gitlab.com/plantd/patches/sample.
