//go:generate protoc --go_out=plugins=grpc:. proto/service.proto
package service

import (
	"log"

	cx "gitlab.com/plantd/plantctl/pkg/context"
	pb "gitlab.com/plantd/plantctl/proto"

	"golang.org/x/net/context"
)

type Manager struct {
	handler cx.Handler
}

func NewManager(handler cx.Handler) *Manager {
	return &Manager{handler}
}

func (m *Manager) Start(ctx context.Context, in *pb.Empty) (*pb.Response, error) {

	m.handler.Start()

	return &pb.Response{
		Code:  200,
		Error: nil,
	}, nil
}

func (m *Manager) Stop(ctx context.Context, in *pb.Empty) (*pb.Response, error) {

	m.handler.Stop()

	return &pb.Response{
		Code:  200,
		Error: nil,
	}, nil
}

func (m *Manager) Restart(ctx context.Context, in *pb.Empty) (*pb.Response, error) {

	m.handler.Restart()

	return &pb.Response{
		Code:  200,
		Error: nil,
	}, nil
}

func (m *Manager) Status(ctx context.Context, in *pb.Empty) (*pb.ServiceResponse, error) {

	return &pb.ServiceResponse{
		Code:     200,
		Error:    nil,
		Services: m.handler.GetStatus(),
	}, nil
}

func (m *Manager) Watch(in *pb.Empty, stream pb.Manager_WatchServer) error {
	// probably makes sense to receive these through a channel
	logEntries := []*pb.LogEntry{{
		Service: "plantd-foo.service",
		Message: "something good happened",
	}, {
		Service: "plantd-bar.service",
		Message: "something bad happened",
	}, {
		Service: "plantd-baz.service",
		Message: "something informative happened",
	}}
	for _, entry := range logEntries {
		if err := stream.Send(entry); err != nil {
			return err
		}
	}
	return nil
}

func (m *Manager) Monitor(in *pb.Empty, stream pb.Manager_MonitorServer) error {
	// probably makes sense to receive these through a channel
	services := []*pb.Service{{
		Target: "plantd-foo.service",
		State:  pb.Service_STOPPED,
	}, {
		Target: "plantd-bar.service",
		State:  pb.Service_RUNNING,
	}}
	for _, service := range services {
		if err := stream.Send(service); err != nil {
			return err
		}
	}
	return nil
}

func (m *Manager) Install(ctx context.Context, in *pb.Empty) (*pb.Response, error) {

	log.Println("Install all the things")

	return &pb.Response{
		Code:  501,
		Error: nil,
	}, nil
}

func (m *Manager) Uninstall(ctx context.Context, in *pb.Empty) (*pb.Response, error) {

	log.Println("Uninstall all the things")

	return &pb.Response{
		Code:  501,
		Error: nil,
	}, nil
}

func (m *Manager) Upgrade(ctx context.Context, in *pb.Empty) (*pb.Response, error) {

	log.Println("Upgrade all the things")

	return &pb.Response{
		Code:  501,
		Error: nil,
	}, nil
}

func (m *Manager) Patch(ctx context.Context, in *pb.PatchRequest) (*pb.Response, error) {

	log.Println("Patch ...")

	return &pb.Response{
		Code:  501,
		Error: nil,
	}, nil
}

func (m *Manager) Deploy(ctx context.Context, in *pb.DeployRequest) (*pb.Response, error) {

	log.Println("Deploy ...")

	return &pb.Response{
		Code:  501,
		Error: nil,
	}, nil
}

func (m *Manager) Ping(ctx context.Context, in *pb.Empty) (*pb.Response, error) {

	log.Println("Ping!")

	return &pb.Response{
		Code:  200,
		Error: nil,
	}, nil
}
