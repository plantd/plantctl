package model

type Unit struct {
	Order   int
	Target  string
	Watch   bool
	Control bool
}
