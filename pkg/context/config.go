package context

import (
	"fmt"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

type server struct {
	Bind string `mapstructure:"bind"`
	Port int    `mapstructure:"port"`
}

// TODO: merge this and util one into model/unit.go
type unit struct {
	Order   int  `mapstructure:"order"`
	Watch   bool `mapstructure:"watch"`
	Control bool `mapstructure:"control"`
}

type Config struct {
	App      string          `mapstructure:"app"`
	Plugin   string          `mapstructure:"plugin"`
	Server   server          `mapstructure:"server"`
	Services map[string]unit `mapstructure:"services"`
	Units    map[string]unit `mapstructure:"plantd-unit@"`
	Modules  map[string]unit `mapstructure:"plantd-module@"`
}

func LoadConfig() (*Config, error) {
	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}

	config := viper.New()
	config.SetConfigName("plantctl")
	config.AddConfigPath("/etc/plantd")
	config.AddConfigPath(".")
	config.AddConfigPath(fmt.Sprintf("%s/.config/plantd", home))

	config.AutomaticEnv()

	err = config.ReadInConfig()
	if err != nil {
		return nil, err
	}

	var c Config

	err = config.Unmarshal(&c)
	if err != nil {
		return nil, err
	}

	return &c, nil
}
