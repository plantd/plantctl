package context

import (
	"context"
	"fmt"

	pb "gitlab.com/plantd/plantctl/proto"

	"github.com/golang/protobuf/jsonpb"
	"google.golang.org/grpc"
)

// Client is used to make service calls over gRPC
type ManagerClient struct {
	Connection pb.ManagerClient
}

// NewClient creates a new Client type
func NewManagerClient(host string, port int) (client *ManagerClient, err error) {
	addr := fmt.Sprintf("%s:%d", host, port)

	var conn *grpc.ClientConn
	conn, err = grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	c := pb.NewManagerClient(conn)

	client = &ManagerClient{
		Connection: c,
	}

	return client, nil
}

func (c *ManagerClient) Ping() (err error) {
	in := &pb.Empty{}
	response, err := c.Connection.Ping(context.Background(), in)
	if err != nil {
		return
	}

	m := &jsonpb.Marshaler{}
	json, err := m.MarshalToString(response)
	if err != nil {
		return
	}

	// Print the response
	fmt.Printf("%s\n", json)

	return nil
}
