package context

import (
	pb "gitlab.com/plantd/plantctl/proto"
)

type Handler interface {
	Restart()
	Start()
	Status()
	GetStatus() []*pb.Service
	Stop()
	Watch()
}
