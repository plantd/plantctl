#!/bin/bash

#/usr/bin/plantctl
#/etc/plantd/plantctl.yaml
#/usr/share/plantctl/plugins/systemd.so
#/lib/systemd/system/plantctl.service

# This script needs to be called from the repository root.
VERSION="$(git describe | sed -e 's/[vV]//g')" # version number cannot start with char.
DESCRIPTION="Command Line Interface (CLI) for plantd"
LICENSE=$"MIT"
URL=$"https://gitlab.com/plantd/plantctl.git"
MAINTAINER=$"Mirko Moeller <mirko.moeller@coanda.ca>"
VENDOR=$""
PKG_NAME="plantctl"
INSTALLDIR="${INSTALLDIR:-"/tmp/stage"}"

if which go; then
  : # colon means do nothing
else
  echo "golang not installed or not reachable"
  exit 1
fi

if which fpm; then
  fpm --input-type dir \
    --output-type deb \
    --name "$PKG_NAME" \
    --version $VERSION \
    --description "$DESCRIPTION" \
    --license "$LICENSE" \
    --url "$URL" \
    --maintainer "$MAINTAINER" \
    --vendor "$VENDOR" \
    --deb-systemd init/plantctl.service \
    --chdir "$INSTALLDIR"
else
  echo "fpm not installed or not reachable"
  exit 1
fi
