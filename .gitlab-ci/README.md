# CI

Mostly just notes for now.

## Packaging

To regenerate the `docker` image for the registry to use in the `.gitlab-ci.yml`
file run this from the project root.

```sh
docker build -t registry.gitlab.com/plantd/planctl/alpine:v1 -f .gitlab-ci/Dockerfile .
docker push registry.gitlab.com/plantd/plantctl/alpine:v1
```
