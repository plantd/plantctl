# Base image: https://hub.docker.com/_/golang/
# create: "docker build -t registry.gitlab.com/plantd/plantctl:fpm -f .gitlab-ci/fpm.Dockerfile ."
FROM golang:1.13.15-buster
#MAINTAINER Tom Depew <tom.depew@coanda.ca>

RUN apt-get update -qq && apt-get install --no-install-recommends -qq -y \
        build-essential \
        libsystemd-dev \
        ruby \
        ruby-dev \
        rubygems \
    && gem install --no-document \
        fpm
