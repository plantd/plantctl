PROJECT := "plantctl"
PREFIX ?= /usr
DESTDIR ?=
CONFDIR = /etc
LIBDIR = /usr/lib
BINARY_NAME := "plantctl"
# Use git tag for version unless the VERSION variable is set
ifeq ($(VERSION),) 
VERSION := $(shell git describe)
endif

M = $(shell printf "\033[34;1m▶\033[0m")
TAG := $(shell git describe --all | sed -e's/.*\///g')

all: build

build: ; $(info $(M) Building project...)
	@go build -a -o target/$(BINARY_NAME) \
	    -ldflags "-X gitlab.com/plantd/plantctl/pkg.VERSION=$(VERSION)"

build-static: ; $(info $(M) Building static binary...)
	@CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build \
		-a -tags netgo -ldflags '-w -extldflags "-static"' \
		-o target/plantctl-static

build-deb: build; $(info $(M) Building DEB package)
	@./.gitlab-ci/package-deb.sh

plugin: plugin-docker plugin-systemd

plugin-docker:
	@go build -buildmode=plugin -o target/docker.so plugins/docker/*.go

plugin-systemd:
	@go build -buildmode=plugin -o target/systemd.so plugins/systemd/*.go

install: ; $(info $(M) Installing plantctl...)
	@install -Dm 755 target/plantctl "$(DESTDIR)$(PREFIX)/bin/plantctl"
	@install -Dm 644 configs/v1/plantctl.yaml "$(DESTDIR)/etc/plantd/plantctl.yaml"
	@install -Dm 644 README.md "$(DESTDIR)$(PREFIX)/share/doc/plantd/README.plantctl"
	@install -Dm 644 LICENSE "$(DESTDIR)$(PREFIX)/share/licenses/plantd/COPYING.plantctl"

install-plugin-docker:
	@install -Dm 644 target/docker.so "$(DESTDIR)$(PREFIX)/share/plantctl/plugins/docker.so"

install-plugin-systemd:
	@install -Dm 644 target/systemd.so "$(DESTDIR)$(PREFIX)/share/plantctl/plugins/systemd.so"

uninstall:
	@rm $(DESTDIR)$(PREFIX)/bin/plantctl
	@rm $(DESTDIR)$(PREFIX)/share/plantctl/plugins/*.so

clean: ; $(info $(M) Removing generated files... )
	@rm -rf bin/

.PHONY: all build build-static build-plugin plugin-systemd install install-plugin-systemd uninstall clean
