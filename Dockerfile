# Build with:
#
#  docker build -t registry.gitlab.com/plantd/plantctl:v1 --build-arg VERSION=$(git describe) .

FROM golang:1.13-alpine
MAINTAINER Geoff Johnson <geoff.jay@gmail.com>

ARG VERSION
ENV VERSION ${VERSION}

# Install dependencies
RUN apk update \
    && apk upgrade \
    && apk add --no-cache \
        bash \
        binutils \
    && apk add --no-cache --virtual .build-deps \
        gcc \
        g++ \
        git \
        make \
        pkgconfig \
        alpine-sdk \
        musl-dev \
        util-linux-dev

WORKDIR /app
COPY . .

RUN make \
    && make install \
    && mkdir /etc/plantd \
    && cp configs/v1/plantctl.yaml /etc/plantd/

ENV PLANTCTL_SERVER_BIND 127.0.0.1
ENV PLANTCTL_SERVER_PORT 5211
EXPOSE 5211

CMD [ "/usr/bin/plantctl", "server" ]
