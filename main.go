package main

import (
	"gitlab.com/plantd/plantctl/cmd"
)

func main() {
	cmd.Execute()
}
