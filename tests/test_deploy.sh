#!/bin/bash

TMP=/tmp/plantd/deployments # must be different from cache directory used by plantctl
../target/plantctl deploy --prepare --dotenv ./test_env https://gitlab.com/plantd/deployments/test-private.git
../target/plantctl deploy --prepare --dotenv ./test_env --no-deploy-token https://gitlab+deploy-token-597422:BX_r-25aqHhDbwY_sZee@gitlab.com/plantd/deployments/test-private.git
../target/plantctl deploy --prepare  --dotenv ./test_env test
../target/plantctl deploy --prepare  --dotenv ./test_env --no-deploy-token test
../target/plantctl deploy --prepare  --dotenv ./test_env --no-deploy-token https://gitlab.com/plantd/deployments/test.git
[ ! -d  $TMP ] && mkdir -p $TMP
[ ! -d  $TMP/test-cloned ] && git clone https://gitlab.com/plantd/deployments/test.git $TMP/test-cloned
../target/plantctl deploy --prepare  --dotenv ./test_env $TMP/test-cloned
../target/plantctl deploy --prepare  --dotenv ./test_env --no-deploy-token $TMP/test-cloned

