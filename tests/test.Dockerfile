FROM registry.gitlab.com/plantd/plantctl:fpm
Run apt-get --allow-releaseinfo-change update &&\
	apt-get install -y -qq vim python3 python3-pip &&\
	python3 -m pip install --upgrade pip &&\
	python3 -m pip install ansible  
RUN mkdir -p /plantctl
COPY . /plantctl
WORKDIR /plantctl
