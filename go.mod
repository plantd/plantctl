module gitlab.com/plantd/plantctl

go 1.13

require (
	github.com/fatih/color v1.9.0 // indirect
	github.com/godbus/dbus v5.0.0+incompatible // indirect
	github.com/golang/protobuf v1.3.3
	github.com/gosuri/uitable v0.0.4 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/logrusorgru/aurora v2.0.3+incompatible // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.6
	github.com/spf13/viper v1.6.2
	golang.org/x/net v0.0.0-20200222125558-5a598a2470a0
	google.golang.org/grpc v1.27.1
	gopkg.in/src-d/go-git.v4 v4.13.1
)
